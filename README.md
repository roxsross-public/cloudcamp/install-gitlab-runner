# install-gitlab-runner

¿Qué es Gitlab? Es una plataforma colaborativa de desarrollo de software para proyectos DevOps grandes. Proporciona capacidades DevOps de extremo a extremo para cada etapa del ciclo de vida de desarrollo de software.

¿Qué es Gitlab Runner? Es simplemente un ejecutor de trabajos o pipelines que se crea en un proyecto de Gitlab. Puede ser una shell, Docker, Kubernetes, etc.

Comencemos con la configuración de nuestro setup:
➡️ Inicia sesión en tu cuenta de Gitlab y haz clic en crear un proyecto.
![](./img/1.jpg)

➡️ Haz clic en Crear proyecto en blanco y da un nombre al proyecto, luego crea el proyecto.
![](./img/2.jpg)

➡️ Verás una interfaz algo así.
![](./img/3.jpg)

➡️ En el lado izquierdo, pasa el mouse sobre configuración y luego haz clic en CI/CD.
![](./img/4.jpg)

➡️ Haz clic en el botón de Expandir de Runners.
![](./img/5.jpg)

➡️ Desactiva los runners compartidos y copia el token de registro del runner específico.
![](./img/6.jpg)

➡️ Inicia sesión en tu cuenta de AWS y ve al Panel de EC2.

➡️ Lanza una instancia de EC2. 

Hasta ahora, tendrás tu instancia de EC2 así.
![](./img/8.jpg)

➡️ Ahora conecta a tu instancia de EC2.

➡️ Cambia al usuario root.

➡️ Como Gitlab primero clona el código en el runner, necesitas instalar git en la instancia de EC2.

➡️ Ahora ejecuta los siguientes comandos:

```
# Descarga el binario para tu sistema
curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64

# Dale permiso para ejecutar
chmod +x /usr/local/bin/gitlab-runner

# Crea un usuario GitLab Runner
useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

# Instala y ejecuta como un servicio
gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
gitlab-runner start
```

Nota: Si ves un error como "Comando GitLab no encontrado", simplemente cambia la ubicación de /usr/local/bin/gitlab-runner a /usr/bin/gitlab-runner.

Hemos instalado correctamente el software GitLab Runner en nuestra instancia de EC2. Ahora es hora de registrar nuestro runner con nuestro proyecto.

```
gitlab-runner register --url https://gitlab.com/ --registration-token $REGISTRATION_TOKEN
```

Aquí tenemos que mantener la URL de la instancia, el token de registro y la descripción tal como están y agregar una etiqueta a nuestro runner. Esta etiqueta es realmente importante ya que la usaremos en nuestro pipeline.

Ahora nuestra instancia de EC2 está configurada como un GitLab runner y podrás ver un runner específico en la interfaz de runners de GitLab.

➡️ Para verificar nuestra configuración, ve a la sección de código y crea un pipeline (.gitlab-ci.yml) y commitea el código.

➡️ Ahora el pipeline se ejecutará correctamente en tu instancia de EC2.
![](./img/10.png)


➡️ Para modificaciones
```
concurrent = 1
check_interval = 0
connection_max_age = "15m0s"
shutdown_timeout = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "runner-roxsross"
  url = "https://gitlab.com"
  id = 33414376
  token = "TOKEN"
  token_obtained_at = 2024-03-05T20:43:31Z
  token_expires_at = 0001-01-01T00:00:00Z
  executor = "docker"
  [runners.cache]
    MaxUploadedArchiveSize = 0
  [runners.docker]
    tls_verify = false
    image = "ubuntu"
    privileged = true
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache"]
    shm_size = 0
    network_mtu = 0
  [runners.machine]
    IdleCount = 0
    IdleScaleFactor = 0.0
    IdleCountMin = 0
    MachineDriver = ""
    MachineName = ""
```