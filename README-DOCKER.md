### Guía para Configurar GitLab Runner en tu Máquina

Si bien en la página de GitLab se explica cómo levantar un Runner en una máquina, a veces no queda tan claro el paso a paso y por eso pensé en escribir esta guía para nosotros, los humanos. 😅

En ciertas ocasiones, en pequeñas organizaciones, no suelen tener los Runners suficientes para poder ejecutar varios pipelines a la misma vez y como Dev eso es bastante molesto. Porque tienes que esperar a veces minutos o hasta horas para poder compilar y desplegar lo que acabas de subir al repositorio. 🕑😫

Por eso te invito a que pruebes en levantar tu Runner en tu máquina y así poder desplegar sin esperar.

Para ejecutar esta guía, deberás tener previamente instalado Docker. En caso de no tenerlo, te dejo el enlace de cómo instalar Docker: [Instalar Docker](https://docs.docker.com/engine/install/)

#### Empecemos!!! 🤓

1. **Obtener el Token de Registro:**
   - Desde el menú de la izquierda, dirígete a `Settings` ➡️ `CI/CD`.
   - Luego, realiza el Expand en la opción `Runners`.
   - En la columna `Specific runners`, unas líneas abajo, encontrarás el token de registro.

2. **Levantar el Contenedor de GitLab Runner:**
   ```bash
   docker run \
   -d \
   --name git-runner \
   -v /var/run/docker.sock:/var/run/docker.sock \
   -v /srv/gitlab-runner/config:/etc/gitlab-runner \
   gitlab/gitlab-runner:latest
   ```

3. **Configurar el Contenedor con el Token de Registro:**
   - Luego de levantar el contenedor `git-runner`, procedemos a configurarlo con el token de registro.
   ```bash
   docker exec -it git-runner bash
   ```
   - Una vez dentro del contenedor, ejecuta lo siguiente:
   ```bash
   gitlab-runner register \
   --non-interactive \
   --executor="docker" \
   --docker-image alpine:latest \
   --url "https://gitlab.com/" \
   --registration-token "<token-register>" \
   --description "Docker runner" \
   --run-untagged="true" \
   --locked="false"
   ```
   - Para salir del contenedor, ejecuta:
   ```bash
   exit
   ```

4. **Solución de Errores:**
   - En caso de errores al ejecutar un pipeline, si tienes el siguiente error:
     ```
     ERROR: error durante la conexión: Obtener "http://docker:2375/v1.24/info": marcar tcp: búsqueda de docker en 172.31.0.2:53: no hay tal host
     ```
     Se soluciona actualizando el archivo de configuración del runner, agregando en la etiqueta `volumes` de `[runners.docker]` el volumen que se indicó al arrancar el contenedor `/var/run/docker.sock:/var/run/docker.sock`.
     - El archivo a modificar en nuestro caso sería `/srv/gitlab-runner/config/config.toml`
     - `find /etc/gitlab-runner -name config.toml`


     ```toml
     [[runners]]
       name = "Docker runner"
       url = "https://gitlab.com/"
       token = "<token-register>"
       executor = "docker"
       [runners.custom_build_dir]
       [runners.cache]
         [runners.cache.s3]
         [runners.cache.gcs]
         [runners.cache.azure]
       [runners.docker]
         tls_verify = false
         image = "alpine:latest"
         privileged = false
         disable_entrypoint_overwrite = false
         oom_kill_disable = false
         disable_cache = false
         volumes = ["/cache", "/var/run/docker.sock:/var/run/docker.sock"]
         shm_size = 0
     ```

     Reiniciamos el contenedor para que tome los cambios:
     ```bash
     docker restart git-runner
     ```

¡Listo! Ya tendrás tu runner corriendo en tu máquina disponible para tomar cualquier tarea.